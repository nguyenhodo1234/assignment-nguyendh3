﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class updateDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 192, DateTimeKind.Local).AddTicks(4261),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(8709));

            migrationBuilder.AlterColumn<DateTime>(
                name: "LeaveDate",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 192, DateTimeKind.Local).AddTicks(3979),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(8472));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 189, DateTimeKind.Local).AddTicks(8344),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 262, DateTimeKind.Local).AddTicks(2581));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 189, DateTimeKind.Local).AddTicks(7537),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 262, DateTimeKind.Local).AddTicks(1749));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 191, DateTimeKind.Local).AddTicks(7479),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(656));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 191, DateTimeKind.Local).AddTicks(7235),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(439));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(8709),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 192, DateTimeKind.Local).AddTicks(4261));

            migrationBuilder.AlterColumn<string>(
                name: "LeaveDate",
                table: "LeaveRequest",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(8472),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 192, DateTimeKind.Local).AddTicks(3979));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 262, DateTimeKind.Local).AddTicks(2581),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 189, DateTimeKind.Local).AddTicks(8344));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 262, DateTimeKind.Local).AddTicks(1749),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 189, DateTimeKind.Local).AddTicks(7537));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(656),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 191, DateTimeKind.Local).AddTicks(7479));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(439),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 23, 10, 52, 37, 191, DateTimeKind.Local).AddTicks(7235));
        }
    }
}

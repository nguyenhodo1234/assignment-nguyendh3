﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class updateTblLeaveRequests : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(8709),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(7770));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(8472),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(7540));

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "LeaveRequest",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 262, DateTimeKind.Local).AddTicks(2581),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 220, DateTimeKind.Local).AddTicks(9545));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 262, DateTimeKind.Local).AddTicks(1749),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 220, DateTimeKind.Local).AddTicks(8405));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(656),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(1175));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(439),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(957));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "LeaveRequest");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(7770),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(8709));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(7540),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(8472));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 220, DateTimeKind.Local).AddTicks(9545),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 262, DateTimeKind.Local).AddTicks(2581));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 220, DateTimeKind.Local).AddTicks(8405),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 262, DateTimeKind.Local).AddTicks(1749));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(1175),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(656));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(957),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 42, 6, 264, DateTimeKind.Local).AddTicks(439));
        }
    }
}

﻿using Model.LeaveRequest;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.LeaveRequest
{
    public interface ILeaveRequestRepository
    {
        TableResponse<LeaveRequestViewModel> GetListLeaveRequest(SearchLeaveRequestModel search);
        Response<string> CreateLeaveRequest(LeaveRequestModel model);
        Response<LeaveRequestModel> GetLeaveRequestById(LeaveRequestModel model);
        Response<string> UpdateStatusLeaveRequest(LeaveRequestModel model);
        Response<string> UpdateLeaveRequest(LeaveRequestModel model);
        Response<string> DeleteLeaveRequest(LeaveRequestModel model);
    }
}

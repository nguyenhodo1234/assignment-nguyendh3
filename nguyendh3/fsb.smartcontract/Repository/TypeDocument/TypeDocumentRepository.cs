﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Requests;
using Model.Response;
using Model.TypeDocument;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.TypeDocument
{
    public class TypeDocumentRepository : Repository<TypeDocumentRepository>, ITypeDocumentRepository
    {
        private readonly IConfiguration _configuration;
        public TypeDocumentRepository(
            ILogger<TypeDocumentRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
            _configuration = configuration;
        }

        public TableResponse<TypeDocumentViewModel> GetListTypeDocument(SearchCompanyModel search)
        {
            TableResponse<TypeDocumentViewModel> result = new TableResponse<TypeDocumentViewModel>();
            result.Draw = search.Draw;

            try
            {
                var data = _context.TypeDocuments.Where(x => x.IsDelete == false).Select(x => new TypeDocumentViewModel
                {
                    Id = x.Id,
                    TypeDocumentCode = x.TypeDocumentCode,
                    TypeDocumentName = x.TypeDocumentName
                }).ToList();

                if (search.SearchValue != null)
                {
                    data = data.Where(x => x.TypeDocumentName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                var cnt = data.Count();
                result.Data = data.OrderBy(x => x.Id).Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                result.Code = StatusCodes.Status500InternalServerError;
                result.Message = "Xảy ra lỗi khi lấy danh sách loại tài liệu!";
            }
            return result;
        }

        public Response<string> CreateTypeDocument(TypeDocumentModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.TypeDocumentName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên loại tài liệu không được bỏ trống!";
                    return res;
                }

                if (string.IsNullOrEmpty(model.TypeDocumentCode))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Mã loại tài liệu không được bỏ trống!";
                    return res;
                }

                var data = _context.TypeDocuments.FirstOrDefault(x => x.TypeDocumentCode == model.TypeDocumentCode.TrimEnd().TrimStart());
                if (data != null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Loại tài liệu đã tồn tại!";
                    return res;
                }

                Entity.Entity.TypeDocument typeDocument = new Entity.Entity.TypeDocument();
                typeDocument.TypeDocumentName = model.TypeDocumentName;
                typeDocument.TypeDocumentCode = model.TypeDocumentCode;


                _context.TypeDocuments.Add(typeDocument);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Thêm loại tài liệu thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi thêm tên loại tài liệu!";
            }

            return res;
        }

        public Response<string> DeleteTypeDocument(TypeDocumentModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                var typeDocument = _context.TypeDocuments.FirstOrDefault(x => x.Id == model.Id && x.IsDelete == false);
                if (typeDocument == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại loại tài liệu, không thể xóa!";
                    return res;
                }
                typeDocument.IsDelete = true;

                _context.TypeDocuments.Update(typeDocument);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Xóa loại tài liệu thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi xóa loại tài liệu";
            }

            return res;
        }

        public Response<TypeDocumentModel> GetTypeDocumentById(TypeDocumentModel model)
        {
            Response<TypeDocumentModel> res = new Response<TypeDocumentModel>();

            try
            {
                var query = (from a in _context.TypeDocuments
                             where a.IsDelete == false && a.Id == model.Id
                             select new
                             {
                                 a
                             }).ToList();

                var group = query.GroupBy(x => x.a.Id).Select(group => new TypeDocumentModel
                {
                    Id = group.Key,
                    TypeDocumentCode = group.ToList().FirstOrDefault().a.TypeDocumentCode,
                    TypeDocumentName = group.ToList().FirstOrDefault().a.TypeDocumentName
                }).ToList();

                res.Code = StatusCodes.Status200OK;
                res.Data = group.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin loại tài liệu!";
            }

            return res;
        }

        public Response<string> UpdateTypeDocument(TypeDocumentModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.TypeDocumentCode))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Mã loại tài liệu không được bỏ trống!";
                    return res;
                }

                if (string.IsNullOrEmpty(model.TypeDocumentName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên loại tài liệu không được bỏ trống!";
                    return res;
                }

                var typeDocument = _context.TypeDocuments.FirstOrDefault(x => x.IsDelete == false && x.Id == model.Id);
                if (typeDocument == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại loại tài liệu, không thể cập nhật!";
                    return res;
                }

                typeDocument.TypeDocumentName = model.TypeDocumentName;
                typeDocument.TypeDocumentCode = model.TypeDocumentCode;
                _context.TypeDocuments.Update(typeDocument);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Sửa loại tài liệu thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi sửa loại tài liệu!";
            }

            return res;
        }

        public List<SelectListItem> GetListTypeDocumentForCombo()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                var _data = _context.TypeDocuments.Where(x => x.IsDelete == false)
                    .Select(typeDocument => new SelectListItem
                    {
                        Value = typeDocument.Id.ToString(),
                        Text = typeDocument.TypeDocumentName
                    }).ToList();
                list = _data;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi lấy list");
            }
            return list;
        }
    }
}

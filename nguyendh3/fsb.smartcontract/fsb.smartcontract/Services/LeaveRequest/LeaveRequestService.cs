﻿using fsb.smartcontract.Api;
using fsb.smartcontract.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Model.LeaveRequest;
using Model.Requests;
using Model.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace fsb.smartcontract.Services.LeaveRequest
{
    public class LeaveRequestService : Service<ILeaveRequestService>, ILeaveRequestService
    {
        public LeaveRequestService(
           CallWebAPI api,
           ILogger<LeaveRequestService> logger,
           IHttpClientFactory factory,
           IHttpContextAccessor context
           ) : base(api, logger, factory, context)
        {
        }

        public TableResponse<LeaveRequestViewModel> GetListLeaveRequest(SearchLeaveRequestModel search)
        {
            TableResponse<LeaveRequestViewModel> res = new TableResponse<LeaveRequestViewModel>();

            try
            {
                string url = Path.LEAVEREQUEST_DATATABLE;
                res = _api.Post<TableResponse<LeaveRequestViewModel>>(url, search);
                if (res.Code != StatusCodes.Status200OK)
                {
                    res.RecordsFiltered = 0;
                    res.RecordsTotal = 0;
                    res.Draw = search.Draw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }

            return res;
        }

        public List<SelectListItem> GetListClassForCombo()
        {
            var response = Client.GetAsync(Path.CLASS_FORCOMBO).Result;
            string jData = response.Content.ReadAsStringAsync().Result;
            var comboData = JsonConvert.DeserializeObject<List<SelectListItem>>(jData);
            return comboData;
        }

        public Response<string> CreateLeaveRequest(LeaveRequestModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.LEAVEREQUEST_CREATE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<LeaveRequestModel> GetLeaveRequestById(LeaveRequestModel model)
        {
            Response<LeaveRequestModel> res = new Response<LeaveRequestModel>();
            try
            {
                string url = Path.LEAVEREQUEST_BYID;
                res = _api.Post<Response<LeaveRequestModel>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<string> UpdateStatusLeaveRequest(LeaveRequestModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.LEAVEREQUEST_UPDATESTATUS;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<string> UpdateLeaveRequest(LeaveRequestModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.LEAVEREQUEST_UPDATE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<string> DeleteLeaveRequest(LeaveRequestModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.LEAVEREQUEST_DELETE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }
    }
}

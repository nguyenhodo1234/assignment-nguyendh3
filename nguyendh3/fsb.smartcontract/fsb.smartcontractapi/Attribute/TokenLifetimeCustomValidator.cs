﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontractapi.Attribute
{
    public class TokenLifetimeCustomValidator
    {
        public static bool Validate(
        DateTime? notBefore,
        DateTime? expires,
        SecurityToken tokenToValidate,
        TokenValidationParameters @param
    )
        {
            return (expires != null && expires > DateTime.UtcNow);
        }
    }
}

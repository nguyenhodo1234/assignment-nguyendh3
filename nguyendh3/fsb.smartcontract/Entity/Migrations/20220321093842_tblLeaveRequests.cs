﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class tblLeaveRequests : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 220, DateTimeKind.Local).AddTicks(9545),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 17, 5, 417, DateTimeKind.Local).AddTicks(2322));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 220, DateTimeKind.Local).AddTicks(8405),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 17, 5, 417, DateTimeKind.Local).AddTicks(1564));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(1175),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 17, 5, 418, DateTimeKind.Local).AddTicks(9363));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(957),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 17, 5, 418, DateTimeKind.Local).AddTicks(9157));

            migrationBuilder.CreateTable(
                name: "LeaveRequest",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    StudentName = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ClassId = table.Column<int>(type: "int", nullable: false),
                    Title = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    LeaveDate = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    LeaveType = table.Column<int>(type: "int", nullable: false),
                    ReasonLeave = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreatedTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(7540)),
                    UpdatedTime = table.Column<DateTime>(type: "datetime(6)", nullable: false, defaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(7770)),
                    CreatedBy = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    UpdatedBy = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    IsDelete = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaveRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeaveRequest_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_LeaveRequest_ClassId",
                table: "LeaveRequest",
                column: "ClassId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LeaveRequest");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 17, 5, 417, DateTimeKind.Local).AddTicks(2322),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 220, DateTimeKind.Local).AddTicks(9545));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Company",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 17, 5, 417, DateTimeKind.Local).AddTicks(1564),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 220, DateTimeKind.Local).AddTicks(8405));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 17, 5, 418, DateTimeKind.Local).AddTicks(9363),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(1175));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "Class",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 3, 21, 16, 17, 5, 418, DateTimeKind.Local).AddTicks(9157),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 3, 21, 16, 38, 42, 223, DateTimeKind.Local).AddTicks(957));
        }
    }
}

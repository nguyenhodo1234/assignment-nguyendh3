﻿using fsb.smartcontract.Api;
using fsb.smartcontract.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Model.Requests;
using Model.Response;
using Model.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace fsb.smartcontract.Services.Role
{
    public class RoleService : Service<IRoleService>, IRoleService
    {
        public RoleService(
           CallWebAPI api,
           ILogger<RoleService> logger,
           IHttpClientFactory factory,
           IHttpContextAccessor context
           ) : base(api, logger, factory, context)
        {
        }

        public TableResponse<RoleViewModel> GetListRole(SearchRoleModel search)
        {
            TableResponse<RoleViewModel> res = new TableResponse<RoleViewModel>();

            try
            {
                string url = Path.ROLE_DATATABLE;
                res = _api.Post<TableResponse<RoleViewModel>>(url, search);
                if (res.Code != StatusCodes.Status200OK)
                {
                    res.RecordsFiltered = 0;
                    res.RecordsTotal = 0;
                    res.Draw = search.Draw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }

            return res;
        }

        public Response<string> CreateRole(RoleModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.ROLE_CREATE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<string> DeleteRole(RoleModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.ROLE_DELETE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<RoleModel> GetRoleById(RoleModel model)
        {
            Response<RoleModel> res = new Response<RoleModel>();
            try
            {
                string url = Path.ROLE_BYID;
                res = _api.Post<Response<RoleModel>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }

        public Response<string> UpdateRole(RoleModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.ROLE_UPDATE;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Company;
using Model.Requests;
using Model.Response;
using Repository.Company;

namespace fsb.smartcontractapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : BaseController
    {
        private readonly ICompanyRepository _companyRepository;

        public CompanyController(
            ICompanyRepository companyRepository,
            ILogger<CompanyController> logger,
            IConfiguration configuration
            ) : base(configuration, logger)
        {
            _companyRepository = companyRepository;
        }

        [Route("GetListCompany")]
        [HttpPost]
        public TableResponse<CompanyViewModel> GetListCompany(SearchCompanyModel search)
        {
            return _companyRepository.GetListCompany(search);
        }

        [Route("CreateCompany")]
        [HttpPost]
        //[CustomRole(RoleConstants.ADMIN)]
        public Response<string> CreateCompany(CompanyModel model)
        {
            return _companyRepository.CreateCompany(model);
        }

        [Route("DeleteCompany")]
        [HttpPost]
        //[CustomRole(RoleConstants.ADMIN)]
        public Response<string> DeleteCompany(CompanyModel model)
        {
            return _companyRepository.DeleteCompany(model);
        }

        [Route("GetCompanyById")]
        [HttpPost]
        //[CustomRole(RoleConstants.ADMIN, RoleConstants.ACCOUNTANT_HO, RoleConstants.ACCOUNTANT_BP)]
        public Response<CompanyModel> GetCompanyById(CompanyModel model)
        {
            return _companyRepository.GetCompanyById(model);
        }

        [Route("UpdateCompany")]
        [HttpPost]
        //[CustomRole(RoleConstants.ADMIN)]
        public Response<string> UpdateCompany(CompanyModel model)
        {
            return _companyRepository.UpdateCompany(model);
        }

        [Route("GetListCompanyForCombo")]
        [HttpGet]
        public IActionResult GetListCompanyForCombo()
        {
            var comboData = _companyRepository.GetListCompanyForCombo();
            return Ok(comboData);
        }
    }
}

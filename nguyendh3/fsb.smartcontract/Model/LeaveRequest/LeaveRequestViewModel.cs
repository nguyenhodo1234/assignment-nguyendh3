﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.LeaveRequest
{
    public class LeaveRequestViewModel : ModelBase
    {
        public int Id { get; set; }
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        public string Title { get; set; }
        public DateTime LeaveDate { get; set; }
        public int LeaveType { get; set; }
        public string ReasonLeave { get; set; }
        public int Status { get; set; }

        public string LeaveTypeName { get; set; }
        public string StatusName { get; set; }
    }
}

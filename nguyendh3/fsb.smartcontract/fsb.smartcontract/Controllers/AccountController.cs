﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Common;
using Common.Constants;
using fsb.smartcontract.Services.Account;
using fsb.smartcontract.Services.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model.UserInfo;
using NToastNotify;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace fsb.smartcontract.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;
        private readonly ILogger<AccountController> _logger;
        private readonly IToastNotification _toastNotification;
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AccountController(
            ILogger<AccountController> logger,
            IAccountService accountService,
            IHttpClientFactory factory,
            IToastNotification toastNotification,
            IUserService userService,
            IHttpContextAccessor httpContextAccessor
            ) : base(factory)
        {
            _accountService = accountService;
            _logger = logger;
            _toastNotification = toastNotification;
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]

        public IActionResult ActionLogin(LoginModel model)
        {
            model.Ip = ip;
            var res = _accountService.Login(model);

            if (res.Code == StatusCodes.Status200OK)
            {
                token = res.Data;
                var userInfo = new UserViewModel();
                userInfo.UserName = model.UserName;
                var userInfoData = _userService.GetUserByUserName(userInfo);
                if (userInfoData != null && userInfoData.Data != null)
                {
                    userInfo.Id = userInfoData.Data.Id;
                    userInfo.FullName = GetPlainText(userInfoData.Data.FullName);
                    userInfo.UserRoleName = userInfoData.Data.UserRoleName;
                    userInfo.UserRoleNormalizedName = userInfoData.Data.UserRoleNormalizedName;
                    userInfo.IsReportBudget = userInfoData.Data.IsReportBudget;
                    userInfo.IsUploadBudget = userInfoData.Data.IsUploadBudget;
                }

                HttpContext.Session.SetObject<UserViewModel>(LoginConstant.PERSON_INFO_SESSION, userInfo);
                _SessionUser = userInfo;

                ////Lấy config
                //var config = _userService.GetConfig();
                //_Config = config.Data;
                _toastNotification.AddSuccessToastMessage(res.Message);

                string authId = GenerateAuthId();
                HttpContext.Session.SetString("AuthId", authId);
                //CookieOptions options = new CookieOptions()
                //{
                //    Path = "/",
                //    HttpOnly = true,
                //    Secure = true,
                //    SameSite = SameSiteMode.Strict
                //};
                HttpContext.Response.Cookies.Append("AuthCookie", authId);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);

            }
            return Json(res);
        }

        [Route("dang-xuat")]
        public IActionResult ActionLogout()
        {
            clearCookie();
            return RedirectToAction("Index", "Account");
        }

        public IActionResult GetUserInfo()
        {
            var userInfo = HttpContext.Session.GetObject<UserViewModel>(LoginConstant.PERSON_INFO_SESSION);
            return Json(userInfo);
        }
    }
}
